-- preparing oauth data
insert into external_resource(id, name, client_id, client_secret) values(1, 'Test ER', 'cno947t47t9tc984p9s84', 'a0wcn789tp029r123rdghjfznzpc83r48t2twe');
insert into users(id, name, password, email) values(1, 'Test User', '1', 'a@b.c');

insert into oauth_token(id, access_token, expires_in, refresh_token, token_type, external_resource_id, user_id)
values(1, 'c6a95d32-2873-4fb0-bce0-485987d30015', 1882757968231, '81a225ab-b3d9-4944-bfdc-7edd9ff37499', 'Bearer', 1, 1);
