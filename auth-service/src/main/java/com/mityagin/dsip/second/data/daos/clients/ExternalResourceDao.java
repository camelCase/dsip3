package com.mityagin.dsip.second.data.daos.clients;

import com.mityagin.dsip.second.data.entities.clients.ExternalResource;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by Nikita on 12/12/2016.
 */
@Transactional
public interface ExternalResourceDao extends CrudRepository<ExternalResource, Long> {

    ExternalResource findByClientId(String clientId);
}