package com.mityagin.dsip.second.data.entities.clients;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Nikita on 12/12/2016.
 */
@Data
@ToString(exclude = {"id", "code"})
@Entity
@Table(name = "oauth_code")
public class OAuthCode { // TODO: expiration
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull
    @OneToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "external_resource_id")
    private ExternalResource externalResource;

    @NotNull
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    private String code;

    @NotNull
    @Column(name = "redirect_url")
    private String redirectUrl;
}
