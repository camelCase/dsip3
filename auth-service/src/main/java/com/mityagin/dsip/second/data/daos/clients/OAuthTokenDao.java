package com.mityagin.dsip.second.data.daos.clients;

import com.mityagin.dsip.second.data.entities.clients.OAuthToken;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by Nikita on 12/12/2016.
 */
@Transactional
public interface OAuthTokenDao extends CrudRepository<OAuthToken, Long> {

    OAuthToken findByAccessToken(String accessToken);

    OAuthToken findByRefreshToken(String refreshToken);

}
