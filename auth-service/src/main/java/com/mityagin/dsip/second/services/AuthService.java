package com.mityagin.dsip.second.services;

import com.mityagin.dsip.second.data.daos.clients.ExternalResourceDao;
import com.mityagin.dsip.second.data.daos.clients.OAuthCodeDao;
import com.mityagin.dsip.second.data.daos.clients.OAuthTokenDao;
import com.mityagin.dsip.second.data.daos.clients.UserDao;
import com.mityagin.dsip.second.data.entities.clients.ExternalResource;
import com.mityagin.dsip.second.data.entities.clients.OAuthCode;
import com.mityagin.dsip.second.data.entities.clients.OAuthToken;
import com.mityagin.dsip.second.data.entities.clients.User;
import com.mityagin.dsip.second.utils.RandomUtils;
import lombok.NonNull;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Nikita on 12/12/2016.
 */
@Log4j
@Service
public class AuthService {

    @Autowired
    private ExternalResourceDao externalResourceDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private OAuthCodeDao oAuthCodeDao;

    @Autowired
    private OAuthTokenDao oAuthTokenDao;

    public ExternalResource addNewExternalResource(@NonNull String name, String defaultUrl) {
        log.debug("Start. name = " + name + ", defaultUrl = " + defaultUrl);

        ExternalResource externalResource = new ExternalResource();
        externalResource.setName(name);
        externalResource.setClientId(RandomUtils.generateOAuth2ClientId());
        externalResource.setClientSecret(
                RandomUtils.generateOAuth2ClientSecret(externalResource.getClientId())
        );
        externalResource.setDefaultUrl(defaultUrl);

        externalResourceDao.save(externalResource);

        log.debug("End. External Resource saved, returning " + externalResource);

        return externalResource;
    }

    public ExternalResource checkClientId(@NonNull String clientId) throws OAuthException {
        log.debug("Start. client_id = " + clientId);

        ExternalResource externalResource = externalResourceDao.findByClientId(clientId);

        if (externalResource == null) {
            throw new OAuthException("Client with specified clientId not found.");
        }

        log.debug("End. Returning External Resource = " + externalResource);

        return externalResource;
    }

    public User signIn(@NonNull String email, @NonNull String password) throws SignInException {
        log.debug("Start. email = " + email);

        User user = userDao.findByEmail(email);

        if (user == null) {
            throw new SignInException("Bad credentials. Try again with correct email and password or sign up.");
        }

        log.debug("User found " + user + ". Checking password.");

        if (!password.equals(user.getPassword())) {
            throw new SignInException("Bad credentials. Try again with correct email and password or sign up.");
        }

        log.debug("End. Correct credentials. Returning user = " + user);

        return user;
    }

    public User signUp(@NonNull String name, @NonNull String email, @NonNull String password)
            throws SignUpException {
        log.debug("Start. name = " + name + ", email = " + email);

        if (userDao.findByEmail(email) != null) {
            throw new SignUpException("User with such email already exist. Try another email.");
        }

        User user = new User();
        user.setEmail(email);
        user.setName(name);
        user.setPassword(password);

        userDao.save(user);

        log.debug("End. User saved. Returning user = " + user);

        return user;
    }

    public OAuthCode getOAuthCode(@NonNull Long userId, @NonNull String clientId, @NonNull String redirectUrl)
            throws OAuthException {
        log.debug("Start. user_id = " + userId + ", clientId = " + clientId + ", redirectUrl = " + redirectUrl);

        User user = userDao.findOne(userId);

        if (user == null) {
            throw new OAuthException("User with userID = " + userId + " doesn't exists.");
        }

        log.debug("Found user = " + user);

        ExternalResource externalResource = externalResourceDao.findByClientId(clientId);

        if (externalResource == null) {
            throw new OAuthException("External Resource with client_id = " + clientId + " doesn't exist.");
        }

        log.debug("Found External Resource = " + externalResource);

        OAuthCode oAuthCode = new OAuthCode();
        oAuthCode.setCode(RandomUtils.generateOAuth2Code(user.getEmail()/*email*/, clientId, redirectUrl));
        oAuthCode.setUser(user);
        oAuthCode.setExternalResource(externalResource);
        oAuthCode.setRedirectUrl(redirectUrl);

        // TODO: check code existence - it has to be only one code per (user & client)
        oAuthCodeDao.save(oAuthCode);

        log.debug("End. Returning oAuthCode = " + oAuthCode);

        return oAuthCode;
    }

    public OAuthCode checkOAuthCode(@NonNull String clientId, String clientSecret, @NonNull String code)
            throws OAuthException {
        log.debug("Start. clientId = " + clientId);

        OAuthCode oAuthCode = oAuthCodeDao.findByCode(code);

        if (oAuthCode == null) {
            throw new OAuthException("OAuth code didn't found. Try to get one another.");
        }

        log.debug("End. Returning found OAuthCode " + oAuthCode);

        return oAuthCode;
    }

    public String getOAuthToken(@NonNull String code, @NonNull String clientId, @NonNull String clientSecret,
                                    @NonNull String redirectUrl) throws OAuthException {
        log.debug("Start. clientId = " + clientId);

        OAuthCode oAuthCode = oAuthCodeDao.findByCode(code);

        if (oAuthCode == null) {
            throw new OAuthException("OAuth code didn't found. Try to get one another.");
        }

        log.debug("OauthCode found. Checking External Resource.");

        ExternalResource externalResource = oAuthCode.getExternalResource();
        if (externalResource == null) {
            throw new IllegalStateException("OAuth Code found but External Resource not. Internal error.");
        }

        log.debug("External Resource found. Checking clientId and Client Secret.");

        if (!clientId.equals(externalResource.getClientId()) ||
                !clientSecret.equals(externalResource.getClientSecret())) {
            throw new OAuthException("ClientId or ClientSecret aren't valid. Try again");
        }

        log.debug("ClientId and Client Secret are valid.");

        OAuthToken oAuthToken = new OAuthToken();
        oAuthToken.setTokenType("Bearer");
        oAuthToken.setAccessToken(RandomUtils.generateOAuth2Token(code, clientId, clientSecret, redirectUrl));
        oAuthToken.setRefreshToken(RandomUtils.generateOAuth2Token(code, clientId, clientSecret, redirectUrl));
        oAuthToken.setExpiresIn(System.currentTimeMillis() + Constants.ACCESS_TOKEN_TTL_MS);
        oAuthToken.setExternalResource(externalResource);
        oAuthToken.setUser(oAuthCode.getUser());

        oAuthTokenDao.save(oAuthToken);

        log.debug("OAuthToken saved = " + oAuthToken);

        oAuthCodeDao.delete(oAuthCode);

        log.debug("End. OAuthCode deleted. Returning saved OAuthToken.");

        return oAuthToken.toJson();
    }

    public String refreshOAuthToken(@NonNull String refreshToken) throws OAuthException {
        log.debug("Start.");
        OAuthToken oAuthToken = oAuthTokenDao.findByRefreshToken(refreshToken);

        if (oAuthToken == null) {
            throw new OAuthException("Refresh Token didn't found. Try to get Access Token");
        }

        log.debug("Refresh Token is valid.");

        if (oAuthToken.getExpiresIn() >= System.currentTimeMillis()) {
            throw new OAuthException("OAuth Access Token is not expired. Use it for access.");
        }

        oAuthToken.setAccessToken(RandomUtils.generateOAuth2TokenByRefreshToken(refreshToken));
        oAuthToken.setRefreshToken(RandomUtils.generateOAuth2TokenByRefreshToken(refreshToken));
        oAuthToken.setExpiresIn(System.currentTimeMillis() + Constants.ACCESS_TOKEN_TTL_MS);

        log.debug("OAuth Token filled with new Access Token, Refresh Token and expiration time.");

        oAuthTokenDao.save(oAuthToken);

        log.debug("End. OAuth Token updated in database.");

        return oAuthToken.toJson();
    }

    public boolean checkOAuthToken(@NonNull String oAuthAccessToken) throws OAuthException {
        log.debug("Start.");

        OAuthToken oAuthToken = oAuthTokenDao.findByAccessToken(oAuthAccessToken);

        if (oAuthToken == null) {
            throw new OAuthException("Unknown Access Token. Try with correct or request for a new.");
        }

        log.debug("Access token recognized = " + oAuthToken);

        if (oAuthToken.getExpiresIn() < System.currentTimeMillis()) {
            throw new OAuthException("OAuth Access Token is expired. Use Refresh Token.");
        }

        log.debug("End. Access Token isn't expired yet. Returning true.");

        return true;
    }

    protected static class Constants {
        public static final Long ACCESS_TOKEN_TTL_MS = 3600000L;
    }

    public class SignUpException extends Exception {
        public SignUpException(String message) {
            super(message);
        }

        public SignUpException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public class SignInException extends Exception {
        public SignInException(String message) {
            super(message);
        }

        public SignInException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public class OAuthException extends Exception {
        public OAuthException(String message) {
            super(message);
        }

        public OAuthException(String message, Throwable cause) {
            super(message, cause);
        }
    }

}

