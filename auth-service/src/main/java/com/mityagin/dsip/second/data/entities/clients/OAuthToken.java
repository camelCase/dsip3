package com.mityagin.dsip.second.data.entities.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Nikita on 12/12/2016.
 */
@Data
@ToString(exclude = {"id", "accessToken", "refreshToken"})
@Entity
@Table(name = "oauth_token")
public class OAuthToken {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "external_resource_id")
    private ExternalResource externalResource;

    @NotNull
    @OneToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    @Column(name = "access_token")
    private String accessToken;

    @NotNull
    @Column(name = "token_type")
    private String tokenType;

    @NotNull
    @Column(name = "refresh_token")
    private String refreshToken;

    @NotNull
    @Column(name = "expires_in")
    private Long expiresIn;

    @Transient
    private static final ObjectMapper objectMapper = new ObjectMapper();

    // TODO: use Jakson default functionality
    public String toJson() {
        ObjectNode json = objectMapper.createObjectNode();
        json.put("access_token", accessToken);
        json.put("token_type", tokenType);
        json.put("refresh_token", refreshToken);
        json.put("expires_in", expiresIn);

        return json.toString();
    }

}