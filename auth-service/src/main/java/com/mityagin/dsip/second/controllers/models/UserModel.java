package com.mityagin.dsip.second.controllers.models;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by Nikita on 12/11/2016.
 */
@Data
public class UserModel {

    private String email;

    private String name;

    private String password;
}
