package com.mityagin.dsip.second.data.daos.clients;

import com.mityagin.dsip.second.data.entities.clients.OAuthCode;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by Nikita on 12/12/2016.
 */
@Transactional
public interface OAuthCodeDao extends CrudRepository<OAuthCode, Long> {

    OAuthCode findByCode(String code);
}