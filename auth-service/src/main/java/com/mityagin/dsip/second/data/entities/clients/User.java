package com.mityagin.dsip.second.data.entities.clients;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.mityagin.dsip.second.controllers.View;
import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by Nikita on 12/11/2016.
 */
@Data
@ToString(exclude = {"id", "password"})
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue
    @JsonView(View.OAuthPrivate.class)
    private Long id;

    @NotNull
    @JsonView(View.OAuthPrivate.class)
    private String email;

    @NotNull
    @JsonView(View.OAuthPrivate.class)
    private String name;

    @NotNull
    @JsonIgnore
//    @JsonView(View.OAuthPrivate.class)
    private String password;
}
