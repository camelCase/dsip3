package com.mityagin.dsip.second.utils;

import java.util.UUID;

/**
 * Created by Nikita on 12/12/2016.
 */
public class RandomUtils {
    public static String generateOAuth2ClientId() {
        return UUID.randomUUID().toString();
    }

    public static String generateOAuth2ClientSecret(String clientId) {
        return UUID.randomUUID().toString();
    }

    public static String generateOAuth2Code(String email, String clientId, String redirectUrl) {
        return UUID.randomUUID().toString();
    }

    public static String generateOAuth2Token(String code, String clientId, String clientSecret, String redirectUrl) {
        return UUID.randomUUID().toString();
    }

    public static String generateOAuth2TokenByRefreshToken(String refreshToken) {
        return UUID.randomUUID().toString();
    }
}
