package com.mityagin.dsip.second.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.mityagin.dsip.second.data.entities.clients.User;
import com.mityagin.dsip.second.services.UsersService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Nikita on 12/25/2016.
 */
@Log4j
@Controller
public class UsersController {

    @Autowired
    UsersService usersService;

    @GetMapping(value = "/api/me", produces = MediaType.APPLICATION_JSON_VALUE)
    @JsonView(View.OAuthPrivate.class)
    public User getUserInfo(@RequestHeader("Authorization") String authorization) {
        log.debug("Start.");

        User user;
        try {
            user = usersService.getUserByAccessToken(authorization);
        } catch (UsersService.UserException e) {
            log.debug("There is a problem getting the user: " + e.getMessage());
            return null;
        }

        return user;
    }

    @GetMapping(value = "/me")
    public String getUserInfoPage(@RequestParam("Authorization") String authorization, Model model) {
        log.debug("Start.");

        User user;
        try {
            user = usersService.getUserByAccessToken(authorization);
        } catch (UsersService.UserException e) {
            log.debug("There is a problem getting the user: " + e.getMessage());

            return "redirect:/error?error=There is a problem getting the user: " + e.getMessage();
        }

        model.addAttribute("user", user);

        return "user";
    }
}
