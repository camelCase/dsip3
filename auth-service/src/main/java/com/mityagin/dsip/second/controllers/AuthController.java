package com.mityagin.dsip.second.controllers;

import com.mityagin.dsip.second.controllers.models.UserModel;
import com.mityagin.dsip.second.data.entities.clients.ExternalResource;
import com.mityagin.dsip.second.data.entities.clients.OAuthCode;
import com.mityagin.dsip.second.data.entities.clients.User;
import com.mityagin.dsip.second.services.AuthService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Nikita on 12/12/2016.
 */

@Log4j
@Controller
@SuppressWarnings("unused")
public class AuthController {

    @Autowired
    AuthService authService;

    // TODO: ADD Sign UP!
    @GetMapping(value = "/authentication")
    public String authentication(
            @ModelAttribute("user") UserModel userModel,
            @RequestParam(value = "client_id", required = false) String clientId,
            @RequestParam(value = "redirect_url", required = false) String oauthRedirectUrl,
            @RequestParam(value = "response_type", required = false) String responseType,
            @RequestParam(value = "error", required = false) String error,
            Model model
    ) {
        log.debug("Start." + (error != null ? "With error message = " + error:""));

        model.addAttribute("user", userModel);

        if (error != null) {
            model.addAttribute("error", error);
        }

        if (clientId != null && oauthRedirectUrl != null && responseType != null) {
            model.addAttribute("client_id", clientId);
            model.addAttribute("redirect_url", oauthRedirectUrl);
            model.addAttribute("response_type", responseType);

            log.debug("OAuth process parameters added to model: client_id = " + clientId);
        }

        log.debug("End. Returning view = sign");

        return "sign";
    }

    @PostMapping(value = "/authentication")
    public String authentication(
            @ModelAttribute("user") UserModel userModel,
            @RequestParam(value = "client_id", required = false) String clientId,
            @RequestParam(value = "redirect_url", required = false) String oauthRedirectUrl,
            @RequestParam(value = "response_type", required = false) String responseType,
            Model model
    ) {
        log.debug("Start.");

        User user;
        try {
            user = authService.signIn(userModel.getEmail(), userModel.getPassword());
        } catch (AuthService.SignInException e) {
            log.debug("Sign In error: " + e.getMessage());

            String redirectUri = "redirect:/authentication?email=" + userModel.getEmail() + "&error=" + e.getMessage();

            log.debug("Redirecting: " + redirectUri + " with user in model: " + userModel);

            return redirectUri;
        }

        log.debug("Successfully signed in as " + userModel.getEmail());

        model.addAttribute("user_id", user.getId());

        log.debug("User id added to model = " + user.getId());

        if ("code".equals(responseType) && clientId != null && oauthRedirectUrl != null) {
            log.debug("Resuming OAuth process.");

            ExternalResource externalResource;
            try {
                 externalResource = authService.checkClientId(clientId);
            } catch (AuthService.OAuthException e) {
                log.debug("There is an error: " + e.getMessage());
                log.debug("End. Redirecting to error view.");

                return "redirect:/error?error=" + e.getMessage();
            }

            log.debug("Client id is recognized.");

            model.addAttribute("external_resource_name", externalResource.getName());
            model.addAttribute("client_id", clientId);
            model.addAttribute("redirect_url", oauthRedirectUrl);
            model.addAttribute("response_type", responseType);

            log.debug("End. Returning view = externalAccess");

            return "externalAccess";
        }

        log.debug("End. Returning view = index");

        return "index";
    }

    @GetMapping(value = "/oauth2/authentication")
    public String oauthCheckAuthentication(
            @RequestParam(value = "client_id") String clientId,
            @RequestParam(value = "redirect_url") String oauthRedirectUrl,
            @RequestParam(value = "response_type") String responseType,
            @RequestParam(value = "user_id") String userId,
            Model model
    ) {
        log.debug("Start. clientId = " + clientId + ", redirectUrl = " + oauthRedirectUrl +
                ", responseType = " + responseType + ", user_id = " + userId);

        try {
            authService.checkClientId(clientId);
        } catch (AuthService.OAuthException e) {
            log.debug("End. Returning error view. Client Id error: " + e.getMessage());

            return "redirect:/error?error=" + e.getMessage();
        }

        log.debug("Client Id is valid.");

        OAuthCode oAuthCode;
        try {
            oAuthCode = authService.getOAuthCode(Long.valueOf(Integer.valueOf(userId)), clientId, oauthRedirectUrl);
        } catch (AuthService.OAuthException e) {
            log.debug("End. Returning error view. Error while generating OAuth code: " + e.getMessage());

            return "redirect:/error?error=" + e.getMessage();
        }

        log.debug("OAuth Code generated. Returning to the specified redirect url: " + oauthRedirectUrl);

        return "redirect:" + oauthRedirectUrl + "?code=" + oAuthCode.getCode();
    }

    @PostMapping(value = "/oauth2/token")
    @ResponseBody public String oauthToken(
            @RequestParam(value = "code") String code,
            @RequestParam(value = "client_id") String clientId,
            @RequestParam(value = "client_secret") String clientSecret,
            @RequestParam(value = "redirect_url") String redirectUrl,
            @RequestParam(value = "grant_type") String grantType, // TODO: PROCESS
            Model model
    ) {
        log.debug("Start.");

        try {
            String token = authService.getOAuthToken(code, clientId, clientSecret, redirectUrl);

            log.debug("End. Returning token.");

            return token;
        } catch (AuthService.OAuthException e) {
            log.debug("Get OAuth Token error: " + e.getMessage());
            log.debug("End. Returning error.");

            return "Error: " + e.getMessage();
        }
    }

    @PostMapping(value = "/oauth2/refreshtoken")
    @ResponseBody public String refreshOauthToken(
            @RequestParam(value = "refresh_token") String refreshToken
    ) {
        log.debug("Start.");

        try {
            String token = authService.refreshOAuthToken(refreshToken);

            log.debug("End. Returning token.");

            return token;
        } catch (AuthService.OAuthException e) {
            log.debug("Refresh OAuth Token error: " + e.getMessage());
            log.debug("End. Returning error.");

            return "Error: " + e.getMessage();
        }
    }

}
