package com.mityagin.dsip.second.services;

import com.mityagin.dsip.second.data.daos.clients.OAuthTokenDao;
import com.mityagin.dsip.second.data.daos.clients.UserDao;
import com.mityagin.dsip.second.data.entities.clients.OAuthToken;
import com.mityagin.dsip.second.data.entities.clients.User;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Nikita on 12/25/2016.
 */
@Log4j
@Service
public class UsersService {

    @Autowired
    UserDao userDao;

    @Autowired
    OAuthTokenDao oAuthTokenDao;

    public User getUserByAccessToken(final String oAuthAccesToken) throws UserException {
        log.debug("Start.");

        OAuthToken oAuthToken = oAuthTokenDao.findByAccessToken(oAuthAccesToken);

        if (oAuthToken == null) {
            log.debug("OAuth Access Token is not found.");
            throw new UserException("OAuth Access Token is not found.");
        }

        log.debug("OAuth Access token found. Returning user");

        return oAuthToken.getUser();
    }

    public class UserException extends Exception {
        public UserException(String message) {
            super(message);
        }

        public UserException(String message, Throwable cause) {
            super(message, cause);
        }
    }

}
