package com.mityagin.dsip.second.data.entities.clients;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Nikita on 12/12/2016.
 */
@Data
@ToString(exclude = {"id", "clientSecret", "oAuthCode"})
@EqualsAndHashCode(exclude = {"oAuthCode"})
@Entity
@Table(name = "external_resource")
public class ExternalResource {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    @Column(name = "client_id")
    private String clientId;

    @NotNull
    @Column(name = "client_secret")
    private String clientSecret;

    @Column(name = "default_url")
    private String defaultUrl;

    @OneToOne(
            orphanRemoval = true,
            cascade = {CascadeType.REMOVE},
            mappedBy = "externalResource")
    private OAuthCode oAuthCode;

}
