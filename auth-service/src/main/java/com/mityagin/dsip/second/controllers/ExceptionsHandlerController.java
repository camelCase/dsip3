package com.mityagin.dsip.second.controllers;

import lombok.extern.log4j.Log4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nikita on 12/13/2016.
 */
@Log4j
@ControllerAdvice
public class ExceptionsHandlerController {

    @ExceptionHandler(Exception.class)
    public String handleError(HttpServletRequest req, Exception ex, Model model) {
        log.error("Request: " + req.getRequestURL() + " raised " + ex);

        model.addAttribute("exception", ex);
        model.addAttribute("url", req.getRequestURL());

        return "error";
    }

    @GetMapping(value = "/error")
    public String error(Model model) {
        log.debug("Returning error view.");

        return "error";
    }
}
