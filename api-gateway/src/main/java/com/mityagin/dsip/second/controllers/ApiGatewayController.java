package com.mityagin.dsip.second.controllers;

import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequest;
import com.mityagin.dsip.second.Constants;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * Created by Nikita on 12/12/2016.
 */

@Log4j
@Controller
@SuppressWarnings("unused")
public class ApiGatewayController {

    @RequestMapping(value = "/authentication")
    public String redirectAuthenticationRequests(HttpServletRequest request, Model model) {
        log.debug("Start. Model: " + model);
        log.debug("End. Redirecting to auth-service");

        log.debug("Query String: " + request.getQueryString());

        return "redirect:" + Constants.USERS_SERVICE_URL + "/authentication?" + request.getQueryString();
    }

    @RequestMapping(value = "/oauth2/**")
    public String redirectAuthRequests(HttpServletRequest request, Model model) {
        log.debug("Start.");

        log.debug("Query String: " + request.getQueryString());

        log.debug("End. Redirecting to auth-service");
        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        return "redirect:" + Constants.USERS_SERVICE_URL + "/oauth2/" + path + "?" + request.getQueryString();
    }

    @RequestMapping(value = "/api/me")
    @ResponseBody public String getUserInfo(HttpServletRequest gotRequest, Model model) {
        log.debug("Start.");

        try {
            HttpResponse<String> httpResponse =
                    composeNewRequest(gotRequest, Constants.USERS_SERVICE_URL + "/api/me")
                            .header("Authorization", gotRequest.getHeader("Authorization")).asString();

            log.debug("Response received, returning body");

            return httpResponse.getBody();
        } catch (UnirestException e) {
            String error = "Can't execute api call. Exception: " + e.getMessage();

            log.debug(e.getStackTrace());
            log.error("Returning error: " + error);

            return error;
        }
    }

    @RequestMapping(value = "/api/**")
    @ResponseBody public String apiCall(HttpServletRequest request, Model model) {
        log.debug("Start.");

        Long userId = checkAuthorization(request.getHeader("Authorization"));

        log.debug("End. User Id received, is : " + (userId == null ? "not" : "") + " null. Executing API Call.");

        return executeApiCall(request, Constants.TASKS_SERVICE_URL, userId);
//        log.debug("Query String: " + request.getQueryString());
//
//        HttpMethod httpMethod = HttpMethod.GET;
//        if ("POST".equals(request.getMethod())) {
//            httpMethod = HttpMethod.POST;
//        } else if ("PATCH".equals(request.getMethod())) {
//            httpMethod = HttpMethod.PATCH;
//        } else if ("DELETE".equals(request.getMethod())) {
//            httpMethod = HttpMethod.DELETE;
//        }
//
//        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
//        String uri = Constants.TASKS_SERVICE_URL + path + "?" + request.getQueryString();
//
//        HttpRequest httpRequest = new HttpRequest(httpMethod, uri);
//        Enumeration<String> headersNames = request.getHeaderNames();
//
//        if (headersNames != null) {
//            while (headersNames.hasMoreElements()) {
//                String headerName = headersNames.nextElement();
//                log.debug("Header: " + headerName);
//                if (!"authorization".equals(headerName) && !"content-length".equals(headerName)) {
//                    httpRequest.header(headerName, request.getHeader(headerName));
//                }
//            }
//        }
//
//        httpRequest.header("X-User", userId.toString());
//
//        try {
//            HttpResponse<String> httpResponse = httpRequest.asString();
//
//            log.debug("Response received, returning body");
//
//            return httpResponse.getBody();
//        } catch (UnirestException e) {
//            String error = "Can't execute api call. Exception: " + e.getMessage();
//
//            log.debug(e.getStackTrace());
//            log.error("Returning error: " + error);
//
////            throw new IllegalStateException("StackTrace+", e);
//            return error;
//        }
    }

    public String executeApiCall(HttpServletRequest gotRequest, String serviceUrl, Long userId) {
        log.debug("Start.");

//        Long userId = checkAuthorization(gotRequest.getHeader("Authorization"));
//
//        log.debug("User Id received, is : " + (userId == null ? "not" : "") + " null");

        String path = (String) gotRequest.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        String uri = serviceUrl + path + "?" + gotRequest.getQueryString();

        try {
            HttpResponse<String> httpResponse =
                    composeNewRequest(gotRequest, uri).header("X-User", userId.toString()).asString();

            log.debug("Response received, returning body");

            return httpResponse.getBody();
        } catch (UnirestException e) {
            String error = "Can't execute api call. Exception: " + e.getMessage();

            log.debug(e.getStackTrace());
            log.error("Returning error: " + error);

            return error;
        }
    }

    private Long checkAuthorization(String authorization) {
        try {
            HttpResponse<JsonNode> response = Unirest.get(Constants.USERS_SERVICE_URL + "/api/me")
                    .header("Authorization", authorization).asJson();

            JsonNode node = response.getBody();

            log.debug("User json: " + node.toString());

            String userId = node.getObject().getJSONObject("user").get("id").toString();

            log.debug("User id parsed: " + userId);

            return Long.valueOf(userId);
        } catch (UnirestException | NullPointerException e) {
            log.error("Error getting user by got token. Exception: " + e.getMessage());

            return null;
        }
    }

    private HttpRequest composeNewRequest(HttpServletRequest request, String uri) {
        HttpMethod httpMethod = HttpMethod.GET;
        if ("POST".equals(request.getMethod())) {
            httpMethod = HttpMethod.POST;
        } else if ("PATCH".equals(request.getMethod())) {
            httpMethod = HttpMethod.PATCH;
        } else if ("DELETE".equals(request.getMethod())) {
            httpMethod = HttpMethod.DELETE;
        }

        HttpRequest httpRequest = new HttpRequest(httpMethod, uri);
        Enumeration<String> headersNames = request.getHeaderNames();

        if (headersNames != null) {
            while (headersNames.hasMoreElements()) {
                String headerName = headersNames.nextElement();
                if (!"authorization".equals(headerName) && !"content-length".equals(headerName)) {
                    httpRequest.header(headerName, request.getHeader(headerName));
                }
            }
        }

        return httpRequest;
    }
}
