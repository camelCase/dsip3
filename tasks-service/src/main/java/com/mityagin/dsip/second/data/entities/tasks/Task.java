package com.mityagin.dsip.second.data.entities.tasks;

import com.fasterxml.jackson.annotation.JsonView;
import com.mityagin.dsip.second.controllers.View;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Nikita on 12/26/2016.
 */

@Data
@ToString(exclude = {"id"})
@Entity
@Table(name = "tasks")
public class Task {
    @Id
    @GeneratedValue
    @JsonView(View.OAuthPrivate.class)
    private Long id;

    @JsonView(View.OAuthPrivate.class)
    private String name;

    @NotNull
    @Column(name = "user_id")
    private Long userId;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "parent_id")
    @JsonView(View.OAuthPrivate.class)
    private Task parent;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "type_id")
    @NotNull
    @JsonView(View.OAuthPrivate.class)
    private TaskType taskType;

    @OneToMany(
            fetch = FetchType.EAGER,
            orphanRemoval = true,
            cascade = {CascadeType.REMOVE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "note_id")
    @JsonView(View.OAuthPrivate.class)
    private List<Parameter> parameters;

    public void updateWith(Task task) {
        this.name = task.name;
        this.userId = task.userId;
        this.parent = task.parent;
        this.taskType = task.taskType;
    }
}
