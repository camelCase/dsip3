package com.mityagin.dsip.second.data.entities.tasks;

import com.fasterxml.jackson.annotation.JsonView;
import com.mityagin.dsip.second.controllers.View;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Nikita on 12/26/2016.
 */
@Data
@ToString(exclude = {"id", "task"})
@Entity
@Table(name = "parameters")
public class Parameter {
    @Id
    @GeneratedValue
    @JsonView(View.OAuthPrivate.class)
    private Long id;

    @NotNull
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "task_id")
    private Task task;

    @NotNull
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "type_id")
    @JsonView(View.OAuthPrivate.class)
    private ParameterType parameterType;

    @JsonView(View.OAuthPrivate.class)
    private String value;

    @JsonView(View.OAuthPrivate.class)
    private Long reference;

    public void updateWith(Parameter parameter) {
        this.task = parameter.task;
        this.parameterType = parameter.parameterType;
        this.value = parameter.value;
        this.reference = parameter.reference;
    }
}
