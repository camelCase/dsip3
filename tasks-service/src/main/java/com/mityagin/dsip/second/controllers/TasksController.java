package com.mityagin.dsip.second.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.mityagin.dsip.second.data.daos.tasks.ParameterDao;
import com.mityagin.dsip.second.data.daos.tasks.TaskDao;
import com.mityagin.dsip.second.data.daos.tasks.TaskTypeDao;
import com.mityagin.dsip.second.data.daos.tasks.ParameterTypeDao;
import com.mityagin.dsip.second.data.entities.tasks.Parameter;
import com.mityagin.dsip.second.data.entities.tasks.Task;
import com.mityagin.dsip.second.data.entities.tasks.TaskType;
import com.mityagin.dsip.second.data.entities.tasks.ParameterType;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Nikita on 12/26/2016.
 */
@Log4j
@Controller
@SuppressWarnings("unused")
public class TasksController {

    @Autowired
    private TaskDao taskDao;

    @Autowired
    private TaskTypeDao taskTypeDao;

    @Autowired
    private ParameterTypeDao parameterTypeDao;

    @Autowired
    private ParameterDao parameterDao;

    @GetMapping(value = "/api/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    @JsonView(View.OAuthPrivate.class)
    public List<Task> getTasksOfUser(
            @RequestHeader(value = "X-User") Long userId,
            @RequestParam(value = "page") int pageNumber,
            @RequestParam(value = "size") int pageSize
    ) {
        log.debug("Start. Page = " + pageNumber + ", size = " + pageSize);

        List<Task> tasks = taskDao.findByUserId(userId);

        log.debug("End. Found " + (tasks != null ? tasks.size() : 0)  + " tasks. Returning with pagination.");

        return (List<Task>) paginate(tasks, pageNumber, pageSize);
    }

    @PostMapping(value = "/api/task", produces = MediaType.APPLICATION_JSON_VALUE)
    @JsonView(View.OAuthPrivate.class)
    public Task addTask(
            @RequestHeader(value = "X-User") Long userId,
            @ModelAttribute("task") Task task
    ) {
        log.debug("Start. Task = " + task);

        task.setUserId(userId);
        task = taskDao.save(task);

        log.debug("End. Returning saved task.");

        return task;
    }

    @PatchMapping(value = "/api/task/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @JsonView(View.OAuthPrivate.class)
    public Task modifyTask(
            @RequestHeader(value = "X-User") Long userId,
            @PathVariable(name = "id") Long id,
            @ModelAttribute("task") Task task
    ) {
        log.debug("Start. Task = " + task);

        task.setUserId(userId);

        Task taskFromDb = taskDao.findOne(id);
        taskFromDb.updateWith(task);
        taskFromDb = taskDao.save(taskFromDb);

        log.debug("End. Returning updated task.");

        return taskFromDb;
    }

    @DeleteMapping(value = "/api/task/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteTask(
            @RequestHeader(value = "X-User") Long userId,
            @PathVariable(name = "id") Long id
    ) {
        log.debug("Start. Task id = " + id);

        taskDao.delete(taskDao.findOne(id));

        log.debug("End. Task Deleted.");
    }

    @GetMapping(value = "/api/task/{id}/parameters", produces = MediaType.APPLICATION_JSON_VALUE)
    @JsonView(View.OAuthPrivate.class)
    public List<Parameter> getParametersOfTask(
            @RequestHeader(value = "X-User") Long userId,
            @PathVariable(name = "id") Long id,
            @RequestParam(value = "page") int pageNumber,
            @RequestParam(value = "size") int pageSize
    ) {
        log.debug("Start. Task id = " + id + ", Page = " + pageNumber + ", size = " + pageSize);

        List<Parameter> parameters = taskDao.findOne(id).getParameters();

        log.debug("End. Found " + (parameters != null ? parameters.size() : 0)  + " parameters. Returning with pagination.");

        return (List<Parameter>) paginate(parameters, pageNumber, pageSize);
    }

    @PostMapping(value = "/api/task/{task}/parameter", produces = MediaType.APPLICATION_JSON_VALUE)
    @JsonView(View.OAuthPrivate.class)
    public Parameter addParameter(
            @RequestHeader(value = "X-User") Long userId,
            @PathVariable(name = "task") Long taskId,
            @ModelAttribute("parameter") Parameter parameter
    ) {
        log.debug("Start. Parameter = " + parameter);

        parameter = parameterDao.save(parameter);

        log.debug("End. Returning saved parameter.");

        return parameter;
    }

    @PatchMapping(value = "/api/task/{task}/parameter/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @JsonView(View.OAuthPrivate.class)
    public Parameter updateParameter(
            @RequestHeader(value = "X-User") Long userId,
            @PathVariable(name = "task") Long taskId,
            @PathVariable(name = "id") Long parameterId,
            @ModelAttribute("parameter") Parameter parameter
    ) {
        log.debug("Start. Task = " + taskId + ", id = " + parameterId + ", Parameter = " + parameter);

        Parameter parameterFromDb = parameterDao.findOne(parameterId);

        log.debug("Parameter found in db = "  + parameterFromDb);

        parameterFromDb.updateWith(parameter);
        parameterFromDb = parameterDao.save(parameterFromDb);

        log.debug("End. Returning updated parameter.");

        return parameterFromDb;
    }

    @DeleteMapping(value = "/api/task/{taskId}/parameter/{parameterId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteParameter(
            @RequestHeader(value = "X-User") Long userId,
            @PathVariable(name = "parameterId") Long parameterId
    ) {
        log.debug("Start. Parameter id = " + parameterId);

        parameterDao.delete(parameterDao.findOne(parameterId));

        log.debug("End. Parameter Deleted.");
    }

    @GetMapping(value = "/api/task/{id}/children", produces = MediaType.APPLICATION_JSON_VALUE)
    @JsonView(View.OAuthPrivate.class)
    public List<Task> getChildrenTask(
            @RequestHeader(value = "X-User") Long userId,
            @PathVariable(name = "id") Long id,
            @RequestParam(value = "page") int pageNumber,
            @RequestParam(value = "size") int pageSize
    ) {
        log.debug("Start. Task id = " + id + ", Page = " + pageNumber + ", size = " + pageSize);

        List<Task> tasks = taskDao.findByParent(taskDao.findOne(id));

        log.debug("End. Found " + (tasks != null ? tasks.size() : 0)  + " tasks. Returning with pagination.");

        return (List<Task>) paginate(tasks, pageNumber, pageSize);
    }

    @DeleteMapping(value = "/api/task/{taskId}/children", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteChildrenTasks(
            @RequestHeader(value = "X-User") Long userId,
            @PathVariable(name = "taskId") Long taskId
    ) {
        log.debug("Start. Task id = " + taskId);


        List<Task> tasks = taskDao.findByParent(taskDao.findOne(taskId));
        for (Task task: tasks) {
            taskDao.delete(task);
        }

        log.debug("End. " + tasks.size() + " tasks deleted.");
    }

    @GetMapping(value = "/api/tasks/types", produces = MediaType.APPLICATION_JSON_VALUE)
    @JsonView(View.OAuthPublic.class)
    public List<TaskType> getTasksTypes(
            @RequestParam(value = "page") int pageNumber,
            @RequestParam(value = "size") int pageSize
    ) {
        log.debug("Start. Page = " + pageNumber + ", size = " + pageSize);

        List<TaskType> taskTypes = new ArrayList<>();
        for (TaskType taskType : taskTypeDao.findAll()) {
            taskTypes.add(taskType);
        }

        log.debug("End. Found " + taskTypes.size() + " tasks types. Returning them with pagination.");

        return (List<TaskType>) paginate(taskTypes, pageNumber, pageSize);
    }

    @GetMapping(value = "/api/tasks/parameters/types", produces = MediaType.APPLICATION_JSON_VALUE)
    @JsonView(View.OAuthPublic.class)
    public List<ParameterType> getParametersTypes(
            @RequestParam(value = "page") int pageNumber,
            @RequestParam(value = "size") int pageSize
    ) {
        log.debug("Start. Page = " + pageNumber + ", size = " + pageSize);

        List<ParameterType> noteParametersTypes = new ArrayList<>();
        for (ParameterType parameterType: parameterTypeDao.findAll()) {
            noteParametersTypes.add(parameterType);
        }

        log.debug("End. Found " + noteParametersTypes.size() + " tasks parameters types. Returning them with pagination.");

        return (List<ParameterType>) paginate(noteParametersTypes, pageNumber, pageSize);
    }


    private static List paginate(List list, int pageNumber, int pageSize) {
        int offset = pageNumber * pageSize;
        if (list != null && offset < list.size()) {
            if (offset + pageSize <= list.size()) {
                return list.subList(offset, offset + pageSize);
            } else {
                return list.subList(offset, list.size());
            }
        }

        return Collections.EMPTY_LIST;
    }

}
