package com.mityagin.dsip.second.data.daos.tasks;

import com.mityagin.dsip.second.data.entities.tasks.TaskType;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by Nikita on 12/27/2016.
 */
@Transactional
public interface TaskTypeDao extends CrudRepository<TaskType, Long> {
}
