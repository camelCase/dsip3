package com.mityagin.dsip.second.data.entities.tasks;

import com.fasterxml.jackson.annotation.JsonView;
import com.mityagin.dsip.second.controllers.View;
import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Nikita on 12/26/2016.
 */
@Data
@ToString(exclude = {"id"})
@Entity
@Table(name = "parameters_types")
public class ParameterType {
    @Id
    @GeneratedValue
    @JsonView(View.OAuthPrivate.class)
    private Long id;

    @JsonView(View.OAuthPrivate.class)
    private String name;
}
