package com.mityagin.dsip.second.data.daos.tasks;

import com.mityagin.dsip.second.data.entities.tasks.Task;
import com.mityagin.dsip.second.data.entities.tasks.Parameter;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Nikita on 12/26/2016.
 */
@Transactional
public interface ParameterDao extends CrudRepository<Parameter, Long> {

    List<Parameter> findByTask(Task Task);
}
