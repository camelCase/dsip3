-- Tasks data
insert into tasks_types(id, name) values(1, 'Text');
insert into tasks_types(id, name) values(2, 'List');
insert into tasks_types(id, name) values(3, 'Container');
insert into tasks_types(id, name) values(4, 'Sub Task');
insert into tasks_types(id, name) values(5, 'Group');
insert into tasks_types(id, name) values(6, 'Class');
insert into tasks_types(id, name) values(7, 'Label');
insert into tasks_types(id, name) values(8, 'Repeat Task');
insert into tasks_types(id, name) values(9, 'Notification');

insert into parameters_types(id, name) values(1, 'Priority');
insert into parameters_types(id, name) values(2, 'Due Date');
insert into parameters_types(id, name) values(3, 'Progress');
insert into parameters_types(id, name) values(4, 'Previous');
insert into parameters_types(id, name) values(5, 'Next');
insert into parameters_types(id, name) values(6, 'Group');
insert into parameters_types(id, name) values(7, 'Class');
insert into parameters_types(id, name) values(8, 'Label');
insert into parameters_types(id, name) values(9, 'Key Word');
insert into parameters_types(id, name) values(10, 'Repeatable');
insert into parameters_types(id, name) values(11, 'Repeat');
insert into parameters_types(id, name) values(12, 'Has Sub Tasks');
